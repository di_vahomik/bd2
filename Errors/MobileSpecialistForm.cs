﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Errors
{
    public partial class MobileSpecialistForm : Form
    {
        private readonly string ConnectionString = Form1.ConnectionString;
        public static string MyTextBoxValue;
        ViewMobileSpecProblemForm form = new ViewMobileSpecProblemForm();
        public MobileSpecialistForm()
        {
            InitializeComponent();
        }

        

        private void OK_BUTTON_Click(object sender, EventArgs e)
        {
            bool success = false;
            string log = login_ms.Text;
            string pas = password_ms.Text;

            var con = new SqlConnection(ConnectionString);
            try
            {
               
                string command = "SELECT * FROM Worker_Table WHERE Login = '"+ log+"' AND Password ='" + pas +"';";
                SqlCommand check = new SqlCommand(command, con);
                con.Open();
                MyTextBoxValue = login_ms.Text;
                using (var dataReader = check.ExecuteReader())
                {
                    success = dataReader.Read();
                }
            }
            finally
            {
                con.Close();
            }

            if (success )
            {
                form.password = pas;
                form.login = log;
                form.ShowDialog();
                this.Close();
            }
            
            else
            {
                MessageBox.Show("Неверный логин или пароль");
            }
        }


        private void CANCEL_BUTTON_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

    }
}
