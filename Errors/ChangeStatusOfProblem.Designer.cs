﻿namespace Errors
{
    partial class ChangeStatusOfProblem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.CStatus_Combo_box = new System.Windows.Forms.ComboBox();
            this.CResume = new System.Windows.Forms.TextBox();
            this.OK_BUTTON = new System.Windows.Forms.Button();
            this.CANCEL_BUTTON = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Новый статус:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Резюме:";
            // 
            // CStatus_Combo_box
            // 
            this.CStatus_Combo_box.FormattingEnabled = true;
            this.CStatus_Combo_box.Items.AddRange(new object[] {
            "accepted for execution",
            "eliminated",
            "delayed",
            "cannot be solved"});
            this.CStatus_Combo_box.Location = new System.Drawing.Point(114, 17);
            this.CStatus_Combo_box.Name = "CStatus_Combo_box";
            this.CStatus_Combo_box.Size = new System.Drawing.Size(175, 21);
            this.CStatus_Combo_box.TabIndex = 2;
            // 
            // CResume
            // 
            this.CResume.Location = new System.Drawing.Point(114, 49);
            this.CResume.Multiline = true;
            this.CResume.Name = "CResume";
            this.CResume.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.CResume.Size = new System.Drawing.Size(175, 67);
            this.CResume.TabIndex = 3;
            // 
            // OK_BUTTON
            // 
            this.OK_BUTTON.Location = new System.Drawing.Point(114, 122);
            this.OK_BUTTON.Name = "OK_BUTTON";
            this.OK_BUTTON.Size = new System.Drawing.Size(75, 23);
            this.OK_BUTTON.TabIndex = 4;
            this.OK_BUTTON.Text = "OK";
            this.OK_BUTTON.UseVisualStyleBackColor = true;
            this.OK_BUTTON.Click += new System.EventHandler(this.OK_BUTTON_Click);
            // 
            // CANCEL_BUTTON
            // 
            this.CANCEL_BUTTON.Location = new System.Drawing.Point(214, 122);
            this.CANCEL_BUTTON.Name = "CANCEL_BUTTON";
            this.CANCEL_BUTTON.Size = new System.Drawing.Size(75, 23);
            this.CANCEL_BUTTON.TabIndex = 5;
            this.CANCEL_BUTTON.Text = "Отмена";
            this.CANCEL_BUTTON.UseVisualStyleBackColor = true;
            this.CANCEL_BUTTON.Click += new System.EventHandler(this.CANCEL_BUTTON_Click);
            // 
            // ChangeStatusOfProblem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(309, 157);
            this.Controls.Add(this.CANCEL_BUTTON);
            this.Controls.Add(this.OK_BUTTON);
            this.Controls.Add(this.CResume);
            this.Controls.Add(this.CStatus_Combo_box);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "ChangeStatusOfProblem";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ChangeStatusOfProblem";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button OK_BUTTON;
        private System.Windows.Forms.Button CANCEL_BUTTON;
        public System.Windows.Forms.ComboBox CStatus_Combo_box;
        public System.Windows.Forms.TextBox CResume;
    }
}