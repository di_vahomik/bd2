﻿namespace Errors
{
    partial class ProfillingAddNewProfil
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Surname_pnew = new System.Windows.Forms.TextBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.Password_pnew = new System.Windows.Forms.TextBox();
            this.Login_pnew = new System.Windows.Forms.TextBox();
            this.Patronymic_pnew = new System.Windows.Forms.TextBox();
            this.name_pnew = new System.Windows.Forms.TextBox();
            this.Score_pnew = new System.Windows.Forms.TextBox();
            this.Skills_pnew = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.OK_BUTTON = new System.Windows.Forms.Button();
            this.CANCEL_BUTTON = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Surname_pnew
            // 
            this.Surname_pnew.Location = new System.Drawing.Point(120, 18);
            this.Surname_pnew.Name = "Surname_pnew";
            this.Surname_pnew.Size = new System.Drawing.Size(126, 20);
            this.Surname_pnew.TabIndex = 0;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // Password_pnew
            // 
            this.Password_pnew.Location = new System.Drawing.Point(120, 122);
            this.Password_pnew.Name = "Password_pnew";
            this.Password_pnew.Size = new System.Drawing.Size(126, 20);
            this.Password_pnew.TabIndex = 2;
            // 
            // Login_pnew
            // 
            this.Login_pnew.Location = new System.Drawing.Point(120, 96);
            this.Login_pnew.Name = "Login_pnew";
            this.Login_pnew.Size = new System.Drawing.Size(126, 20);
            this.Login_pnew.TabIndex = 3;
            // 
            // Patronymic_pnew
            // 
            this.Patronymic_pnew.Location = new System.Drawing.Point(120, 70);
            this.Patronymic_pnew.Name = "Patronymic_pnew";
            this.Patronymic_pnew.Size = new System.Drawing.Size(126, 20);
            this.Patronymic_pnew.TabIndex = 4;
            // 
            // name_pnew
            // 
            this.name_pnew.Location = new System.Drawing.Point(120, 44);
            this.name_pnew.Name = "name_pnew";
            this.name_pnew.Size = new System.Drawing.Size(126, 20);
            this.name_pnew.TabIndex = 5;
            // 
            // Score_pnew
            // 
            this.Score_pnew.Location = new System.Drawing.Point(120, 174);
            this.Score_pnew.Name = "Score_pnew";
            this.Score_pnew.Size = new System.Drawing.Size(126, 20);
            this.Score_pnew.TabIndex = 6;
            // 
            // Skills_pnew
            // 
            this.Skills_pnew.Location = new System.Drawing.Point(120, 148);
            this.Skills_pnew.Name = "Skills_pnew";
            this.Skills_pnew.Size = new System.Drawing.Size(126, 20);
            this.Skills_pnew.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Фамилия";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Имя";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Отчество";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Логин";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(21, 125);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Пароль";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(21, 151);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Навыки";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(21, 177);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Рейтинг";
            // 
            // OK_BUTTON
            // 
            this.OK_BUTTON.Location = new System.Drawing.Point(79, 200);
            this.OK_BUTTON.Name = "OK_BUTTON";
            this.OK_BUTTON.Size = new System.Drawing.Size(92, 24);
            this.OK_BUTTON.TabIndex = 15;
            this.OK_BUTTON.Text = "Добавить";
            this.OK_BUTTON.UseVisualStyleBackColor = true;
            this.OK_BUTTON.Click += new System.EventHandler(this.OK_BUTTON_Click);
            // 
            // CANCEL_BUTTON
            // 
            this.CANCEL_BUTTON.Location = new System.Drawing.Point(177, 200);
            this.CANCEL_BUTTON.Name = "CANCEL_BUTTON";
            this.CANCEL_BUTTON.Size = new System.Drawing.Size(93, 25);
            this.CANCEL_BUTTON.TabIndex = 16;
            this.CANCEL_BUTTON.Text = "Отмена";
            this.CANCEL_BUTTON.UseVisualStyleBackColor = true;
            this.CANCEL_BUTTON.Click += new System.EventHandler(this.CANCEL_BUTTON_Click);
            // 
            // ProfillingAddNewProfil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(282, 235);
            this.Controls.Add(this.CANCEL_BUTTON);
            this.Controls.Add(this.OK_BUTTON);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Skills_pnew);
            this.Controls.Add(this.Score_pnew);
            this.Controls.Add(this.name_pnew);
            this.Controls.Add(this.Patronymic_pnew);
            this.Controls.Add(this.Login_pnew);
            this.Controls.Add(this.Password_pnew);
            this.Controls.Add(this.Surname_pnew);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Name = "ProfillingAddNewProfil";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Добавить мобильного специалиста";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button OK_BUTTON;
        private System.Windows.Forms.Button CANCEL_BUTTON;
        public System.Windows.Forms.TextBox Surname_pnew;
        public System.Windows.Forms.TextBox Password_pnew;
        public System.Windows.Forms.TextBox Login_pnew;
        public System.Windows.Forms.TextBox Patronymic_pnew;
        public System.Windows.Forms.TextBox name_pnew;
        public System.Windows.Forms.TextBox Score_pnew;
        public System.Windows.Forms.TextBox Skills_pnew;
    }
}