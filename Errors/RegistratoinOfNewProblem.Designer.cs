﻿namespace Errors
{
    partial class RegistratoinOfNewProblem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.addP_Customer_Address = new System.Windows.Forms.TextBox();
            this.addP_Customer_Phone = new System.Windows.Forms.TextBox();
            this.addP_Customer_Company = new System.Windows.Forms.TextBox();
            this.addP_Customer_Contact = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.OK_BUTTON = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.addP_Priority = new System.Windows.Forms.ComboBox();
            this.addP_Description = new System.Windows.Forms.TextBox();
            this.addP_ProblemType = new System.Windows.Forms.ComboBox();
            this.addP_Equipment = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.CANCEL_BUTTON = new System.Windows.Forms.Button();
            this.addP_SerialNum = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(24, 317);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(91, 13);
            this.label11.TabIndex = 45;
            this.label11.Text = "Адрес компании";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(24, 291);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(91, 13);
            this.label10.TabIndex = 44;
            this.label10.Text = "Контактный тел.";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(24, 265);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(93, 13);
            this.label9.TabIndex = 43;
            this.label9.Text = "Контактное лицо";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(24, 239);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(58, 13);
            this.label8.TabIndex = 42;
            this.label8.Text = "Компания";
            // 
            // addP_Customer_Address
            // 
            this.addP_Customer_Address.Location = new System.Drawing.Point(130, 314);
            this.addP_Customer_Address.Name = "addP_Customer_Address";
            this.addP_Customer_Address.Size = new System.Drawing.Size(331, 20);
            this.addP_Customer_Address.TabIndex = 41;
            // 
            // addP_Customer_Phone
            // 
            this.addP_Customer_Phone.Location = new System.Drawing.Point(130, 288);
            this.addP_Customer_Phone.Name = "addP_Customer_Phone";
            this.addP_Customer_Phone.Size = new System.Drawing.Size(331, 20);
            this.addP_Customer_Phone.TabIndex = 40;
            // 
            // addP_Customer_Company
            // 
            this.addP_Customer_Company.Location = new System.Drawing.Point(130, 236);
            this.addP_Customer_Company.Name = "addP_Customer_Company";
            this.addP_Customer_Company.Size = new System.Drawing.Size(331, 20);
            this.addP_Customer_Company.TabIndex = 39;
            // 
            // addP_Customer_Contact
            // 
            this.addP_Customer_Contact.Location = new System.Drawing.Point(130, 262);
            this.addP_Customer_Contact.Name = "addP_Customer_Contact";
            this.addP_Customer_Contact.Size = new System.Drawing.Size(331, 20);
            this.addP_Customer_Contact.TabIndex = 38;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(135, 13);
            this.label7.TabIndex = 37;
            this.label7.Text = "Информация о проблеме";
            // 
            // OK_BUTTON
            // 
            this.OK_BUTTON.Location = new System.Drawing.Point(149, 340);
            this.OK_BUTTON.Name = "OK_BUTTON";
            this.OK_BUTTON.Size = new System.Drawing.Size(178, 23);
            this.OK_BUTTON.TabIndex = 36;
            this.OK_BUTTON.Text = "Зарегистрировать";
            this.OK_BUTTON.UseVisualStyleBackColor = true;
            this.OK_BUTTON.Click += new System.EventHandler(this.OK_BUTTON_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 213);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(126, 13);
            this.label6.TabIndex = 35;
            this.label6.Text = "Информация о клиенте";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 186);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 34;
            this.label5.Text = "Приоритет";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 117);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 13);
            this.label4.TabIndex = 33;
            this.label4.Text = "Описание";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 32;
            this.label3.Text = "Тип проблемы";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 13);
            this.label2.TabIndex = 31;
            this.label2.Text = "Серийный номер";
            // 
            // addP_Priority
            // 
            this.addP_Priority.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.addP_Priority.FormattingEnabled = true;
            this.addP_Priority.Location = new System.Drawing.Point(130, 183);
            this.addP_Priority.Name = "addP_Priority";
            this.addP_Priority.Size = new System.Drawing.Size(331, 21);
            this.addP_Priority.TabIndex = 30;
            // 
            // addP_Description
            // 
            this.addP_Description.Location = new System.Drawing.Point(130, 114);
            this.addP_Description.Multiline = true;
            this.addP_Description.Name = "addP_Description";
            this.addP_Description.Size = new System.Drawing.Size(331, 63);
            this.addP_Description.TabIndex = 29;
            // 
            // addP_ProblemType
            // 
            this.addP_ProblemType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.addP_ProblemType.Items.AddRange(new object[] {
            ""});
            this.addP_ProblemType.Location = new System.Drawing.Point(130, 87);
            this.addP_ProblemType.Name = "addP_ProblemType";
            this.addP_ProblemType.Size = new System.Drawing.Size(331, 21);
            this.addP_ProblemType.TabIndex = 28;
            // 
            // addP_Equipment
            // 
            this.addP_Equipment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.addP_Equipment.FormattingEnabled = true;
            this.addP_Equipment.Location = new System.Drawing.Point(130, 32);
            this.addP_Equipment.Name = "addP_Equipment";
            this.addP_Equipment.Size = new System.Drawing.Size(331, 21);
            this.addP_Equipment.TabIndex = 26;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 13);
            this.label1.TabIndex = 25;
            this.label1.Text = "Тип оборудования";
            // 
            // CANCEL_BUTTON
            // 
            this.CANCEL_BUTTON.Location = new System.Drawing.Point(365, 340);
            this.CANCEL_BUTTON.Name = "CANCEL_BUTTON";
            this.CANCEL_BUTTON.Size = new System.Drawing.Size(96, 23);
            this.CANCEL_BUTTON.TabIndex = 46;
            this.CANCEL_BUTTON.Text = "Отмена";
            this.CANCEL_BUTTON.UseVisualStyleBackColor = true;
            this.CANCEL_BUTTON.Click += new System.EventHandler(this.CANCEL_BUTTON_Click);
            // 
            // addP_SerialNum
            // 
            this.addP_SerialNum.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.addP_SerialNum.FormattingEnabled = true;
            this.addP_SerialNum.Location = new System.Drawing.Point(130, 59);
            this.addP_SerialNum.Name = "addP_SerialNum";
            this.addP_SerialNum.Size = new System.Drawing.Size(331, 21);
            this.addP_SerialNum.TabIndex = 47;
            // 
            // RegistratoinOfNewProblem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(477, 377);
            this.Controls.Add(this.addP_SerialNum);
            this.Controls.Add(this.CANCEL_BUTTON);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.addP_Customer_Address);
            this.Controls.Add(this.addP_Customer_Phone);
            this.Controls.Add(this.addP_Customer_Company);
            this.Controls.Add(this.addP_Customer_Contact);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.OK_BUTTON);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.addP_Priority);
            this.Controls.Add(this.addP_Description);
            this.Controls.Add(this.addP_ProblemType);
            this.Controls.Add(this.addP_Equipment);
            this.Controls.Add(this.label1);
            this.Name = "RegistratoinOfNewProblem";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Регистрация новой проблемы";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox addP_Customer_Address;
        public System.Windows.Forms.TextBox addP_Customer_Phone;
        public System.Windows.Forms.TextBox addP_Customer_Company;
        public System.Windows.Forms.TextBox addP_Customer_Contact;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button OK_BUTTON;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.ComboBox addP_Priority;
        public System.Windows.Forms.TextBox addP_Description;
        public System.Windows.Forms.ComboBox addP_ProblemType;
        public System.Windows.Forms.ComboBox addP_Equipment;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button CANCEL_BUTTON;
        public System.Windows.Forms.ComboBox addP_SerialNum;
    }
}