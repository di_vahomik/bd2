﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Errors
{
    public partial class ProfillingAddProfilForm : Form
    {
        private readonly string ConnectionString = Form1.ConnectionString;
        public ProfillingAddProfilForm()
        {
            InitializeComponent();
        }

        private void CANCEL_BUTTON_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void OK_BUTTON_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        public Dictionary<int, string> Data
        {
            set
            {
                cb_linkWorker.DataSource = value.ToArray();
                cb_linkWorker.DisplayMember = "Value";
            }
        }

        public int Id
        {
            get { return ((KeyValuePair<int, string>)cb_linkWorker.SelectedItem).Key; }
        }

        public Dictionary<int, string> ProblemData
        {
            set
            {
                linkP_Priority.DataSource = value.ToArray();
                linkP_Priority.DisplayMember = "Value";
            }
        }

        public int ProblemId
        {
            get { return ((KeyValuePair<int, string>)linkP_Priority.SelectedItem).Key; }
        }
        private void updateWindows()
        {
            var con = new SqlConnection(ConnectionString);
            con.Open();
            var request = "SELECT Skills FROM Worker_Table WHERE Id = " + Id + "";
            var com = new SqlCommand(request, con);
            var last = com.ExecuteScalar();
            linkW_Skills.Text = last.ToString();

            request = "SELECT Score FROM Worker_Table WHERE Id = " + Id + "";
            com = new SqlCommand(request, con);
            last = com.ExecuteScalar();
            linkW_Rating.Text = last.ToString();

            con.Close();
        }

        private void Cb_linkWorker_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateWindows();
        }

        private void ProfillingAddProfilForm_Load(object sender, EventArgs e)
        {
            updateWindows();
        }
    }
}
