﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;

using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Errors
{
    public partial class RegistrForm : Form
    {

        private readonly string ConnectionString = Form1.ConnectionString;
        

        public RegistrForm()
        {
            InitializeComponent();
        }

        private void updateData()
        {
            var request = "SELECT * FROM Problem_Table";
            var adapter = new SqlDataAdapter(request, ConnectionString);
            var table = new DataTable();
            adapter.Fill(table);
            dg_registration.DataSource = table;
            dg_registration.Columns["Id"].Visible = false;
            dg_registration.Columns["Worker_id"].Visible = false;
            dg_registration.Columns["TypeOfEquipment"].HeaderText = "Тип оборудования";
            dg_registration.Columns["SerialNum"].HeaderText = "Серийный номер";
            dg_registration.Columns["TypeOfProblem"].HeaderText = "Тип проблемы";
            dg_registration.Columns["Description"].Visible = false;
            dg_registration.Columns["Priority"].HeaderText = "Приоритет";
            dg_registration.Columns["Customer_Source"].HeaderText = "Клиент";
            dg_registration.Columns["Customer_Contact"].Visible = false;
            dg_registration.Columns["Customer_Phone"].Visible = false;
            dg_registration.Columns["Customer_Adress"].Visible = false;
            dg_registration.Columns["DateOfRegistration"].HeaderText = "Проблема зарегистрирована";
            dg_registration.Columns["Status"].HeaderText = "Статус проблемы";
            dg_registration.Columns["DateOfStatus"].HeaderText = "Статус изменен";
            dg_registration.Columns["Resume"].Visible = false;

            dg_registration.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;

        }

        private void updateWindows()
        {
            var mas = dg_registration.SelectedRows;
            var candidat = mas[0].Cells[0].FormattedValue.ToString();

            var con = new SqlConnection(ConnectionString);
            con.Open();
            var request = "SELECT Description FROM Problem_Table WHERE Id = " + candidat + "";
            var com = new SqlCommand(request, con);
            var last = com.ExecuteScalar();
            p_Description.Text = last.ToString();

            request = "SELECT Resume FROM Problem_Table WHERE Id = " + candidat + "";
            com = new SqlCommand(request, con);
            last = com.ExecuteScalar();
            p_Summary.Text = last.ToString();

            request = "SELECT Customer_Source FROM Problem_Table WHERE Id = " + candidat + "";
            com = new SqlCommand(request, con);
            last = com.ExecuteScalar();
            p_Customer_Company.Text = last.ToString();

            request = "SELECT Customer_Contact FROM Problem_Table WHERE Id = " + candidat + "";
            com = new SqlCommand(request, con);
            last = com.ExecuteScalar();
            p_Customer_Contact.Text = last.ToString();

            request = "SELECT Customer_Phone FROM Problem_Table WHERE Id = " + candidat + "";
            com = new SqlCommand(request, con);
            last = com.ExecuteScalar();
            p_Customer_Phone.Text = last.ToString();

            request = "SELECT Customer_Adress FROM Problem_Table WHERE Id = " + candidat + "";
            com = new SqlCommand(request, con);
            last = com.ExecuteScalar();
            p_Customer_Address.Text = last.ToString();

            con.Close();
        }


        private void RegistrForm_Load(object sender, EventArgs e)
        {
            updateData();
            updateWindows();
        }
       
        private void btn_addProblem_Click(object sender, EventArgs e)
        {
            var form = new RegistratoinOfNewProblem();

            {
                var getReq = "SELECT DISTINCT TypeOfEquipment FROM Problem_Table ";
                var Adapter = new SqlDataAdapter(getReq, ConnectionString);
                var Tbl = new DataTable();
                Adapter.Fill(Tbl);
                form.addP_Equipment.DataSource = Tbl;
                form.addP_Equipment.DisplayMember = "TypeOfEquipment";
            }

            {
                var getReq = "SELECT DISTINCT SerialNum FROM Problem_Table";
                var Adapter = new SqlDataAdapter(getReq, ConnectionString);
                var Tbl = new DataTable();
                Adapter.Fill(Tbl);
                form.addP_SerialNum.DataSource = Tbl;
                form.addP_SerialNum.DisplayMember = "SerialNum";
              
            }

            {
                var getReq = "SELECT DISTINCT Priority FROM Problem_Table";
                var Adapter = new SqlDataAdapter(getReq, ConnectionString);
                var Tbl = new DataTable();
                Adapter.Fill(Tbl);
                form.addP_Priority.DataSource = Tbl;
                form.addP_Priority.DisplayMember = "Priority";

            }
            {
                var getReq = "SELECT DISTINCT TypeOfProblem FROM Problem_Table";
                var Adapter = new SqlDataAdapter(getReq, ConnectionString);
                var Tbl = new DataTable();
                Adapter.Fill(Tbl);
                form.addP_ProblemType.DataSource = Tbl;
                form.addP_ProblemType.DisplayMember = "TypeOfProblem";

            }
            if (form.ShowDialog() == DialogResult.OK)
            {
                DateTime myDateTime = DateTime.Now;
                string sqlFormattedDate = myDateTime.ToString("yyyy-MM-dd HH:mm:ss.fff");

                var con = new SqlConnection(ConnectionString);
                con.Open();
                var request = "INSERT INTO Problem_Table (TypeOfEquipment, SerialNum, TypeOfProblem, Description, Priority, Customer_Source, Customer_Contact, Customer_Phone, Customer_Adress, DateOfRegistration, Status, DateOfStatus)" + " VALUES " + "(N'" + form.addP_Equipment.Text + "', N'" + form.addP_SerialNum.Text + "', N'" + form.addP_ProblemType.Text + "', N'" + form.addP_Description.Text + "', N'" + form.addP_Priority.Text + "', N'" + form.addP_Customer_Company.Text + "', N'" + form.addP_Customer_Contact.Text + "', '" + form.addP_Customer_Phone.Text + "', N'" + form.addP_Customer_Address.Text + "', '" + sqlFormattedDate + "', N'" + "registered" + "', '" + sqlFormattedDate + "')";
                var com = new SqlCommand(request, con);
                com.ExecuteNonQuery();
                con.Close();

                updateData();
            }

        }

        private void dg_registration_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            updateWindows();
        }

        private void Dg_registration_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            updateWindows();
        }

        private void CANCEL_BUTTON_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
