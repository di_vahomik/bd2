﻿namespace Errors
{
    partial class MobileSpecialistForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OK_BUTTON = new System.Windows.Forms.Button();
            this.CANCEL_BUTTON = new System.Windows.Forms.Button();
            this.login_ms = new System.Windows.Forms.TextBox();
            this.password_ms = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // OK_BUTTON
            // 
            this.OK_BUTTON.Location = new System.Drawing.Point(24, 119);
            this.OK_BUTTON.Name = "OK_BUTTON";
            this.OK_BUTTON.Size = new System.Drawing.Size(102, 26);
            this.OK_BUTTON.TabIndex = 0;
            this.OK_BUTTON.Text = "Вход";
            this.OK_BUTTON.UseVisualStyleBackColor = true;
            this.OK_BUTTON.Click += new System.EventHandler(this.OK_BUTTON_Click);
            // 
            // CANCEL_BUTTON
            // 
            this.CANCEL_BUTTON.Location = new System.Drawing.Point(152, 119);
            this.CANCEL_BUTTON.Name = "CANCEL_BUTTON";
            this.CANCEL_BUTTON.Size = new System.Drawing.Size(87, 25);
            this.CANCEL_BUTTON.TabIndex = 1;
            this.CANCEL_BUTTON.Text = "Отмена";
            this.CANCEL_BUTTON.UseVisualStyleBackColor = true;
            this.CANCEL_BUTTON.Click += new System.EventHandler(this.CANCEL_BUTTON_Click);
            // 
            // login_ms
            // 
            this.login_ms.Location = new System.Drawing.Point(98, 37);
            this.login_ms.Name = "login_ms";
            this.login_ms.Size = new System.Drawing.Size(127, 20);
            this.login_ms.TabIndex = 2;
            // 
            // password_ms
            // 
            this.password_ms.Location = new System.Drawing.Point(98, 76);
            this.password_ms.Name = "password_ms";
            this.password_ms.Size = new System.Drawing.Size(127, 20);
            this.password_ms.TabIndex = 3;
            
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(21, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 15);
            this.label1.TabIndex = 4;
            this.label1.Text = "Логин:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(21, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 15);
            this.label2.TabIndex = 5;
            this.label2.Text = "Пароль:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(95, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "Введите:";
            // 
            // MobileSpecialistForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(262, 155);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.password_ms);
            this.Controls.Add(this.login_ms);
            this.Controls.Add(this.CANCEL_BUTTON);
            this.Controls.Add(this.OK_BUTTON);
            this.Name = "MobileSpecialistForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Авторизация Мобильного специалиста";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button OK_BUTTON;
        private System.Windows.Forms.Button CANCEL_BUTTON;
        public System.Windows.Forms.TextBox login_ms;
        public System.Windows.Forms.TextBox password_ms;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}