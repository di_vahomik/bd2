﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Errors
{
    public partial class Form1 : Form
    {

        public readonly static string ConnectionString = global::Errors.Properties.Settings.Default.Database1ConnectionString;

        public Form1()
        {
            InitializeComponent();
        }

        private void Registration_button_Click(object sender, EventArgs e)
        {
            var form = new RegistrForm();
            form.Show();
        }

        private void MobileSpec_button_Click(object sender, EventArgs e)
        {
            var form = new MobileSpecialistForm();
            form.Show();
        }

        private void Profilling_button_Click(object sender, EventArgs e)
        {
            var form = new ProffilingForm();
            form.Show();
        }

        private void MonQuality_button_Click(object sender, EventArgs e)
        {
            var form = new RatingForm();
            form.Show();
        }
    }
}
