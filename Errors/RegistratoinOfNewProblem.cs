﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Errors
{
    public partial class RegistratoinOfNewProblem : Form
    {
        public RegistratoinOfNewProblem()
        {
            InitializeComponent();
        }

        private void OK_BUTTON_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void CANCEL_BUTTON_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        
        public Dictionary<int, string> EquipmentData
        {
            set
            {
                addP_Equipment.DataSource = value.ToArray();
                addP_Equipment.DisplayMember = "Value";
            }

        }
        public int Equipment
        {
            get { return ((KeyValuePair<int, string>)addP_Equipment.SelectedItem).Key; }
        }
        public Dictionary<int, string> SerialNumData
        {
            set
            {
                addP_SerialNum.DataSource = value.ToArray();
                addP_SerialNum.DisplayMember = "Value";
            }
        }
        public int SerialNumD
        {
            get { return ((KeyValuePair<int, string>)addP_SerialNum.SelectedItem).Key; }
            set
            {
                int idx = 0;
                foreach (KeyValuePair<int, string> item in addP_SerialNum.Items)
                {
                    if (item.Key == value) break;
                    idx++;
                }
                addP_SerialNum.SelectedIndex = idx;
            }
        }

        


    }
}
