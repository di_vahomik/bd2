﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Errors
{
    public partial class ViewMobileSpecProblemForm : Form
    {
        private readonly string ConnectionString = Form1.ConnectionString;
        public string password;
        public string login;
        public ViewMobileSpecProblemForm()
        {
            InitializeComponent();
        }
        private void updateData()
        {
            
            var request = "SELECT * FROM Problem_Table FULL OUTER JOIN Worker_Table ON Problem_Table.Worker_Id = Worker_Table.Id WHERE Password ='" + password + "' AND Login='" + login +"';";
            var adapter = new SqlDataAdapter(request, ConnectionString);
            var table = new DataTable();
            adapter.Fill(table);
            dg_profiling.DataSource = table;
            dg_profiling.Columns["Id"].Visible = false;
            dg_profiling.Columns["TypeOfEquipment"].HeaderText = "Тип оборудования";
            dg_profiling.Columns["SerialNum"].HeaderText = "Серийный номер";
            dg_profiling.Columns["TypeOfProblem"].HeaderText = "Тип проблемы";
            dg_profiling.Columns["Description"].Visible = false;
            dg_profiling.Columns["Priority"].HeaderText = "Приоритет";
            dg_profiling.Columns["Customer_Source"].HeaderText = "Источник";
            dg_profiling.Columns["Customer_Contact"].Visible = false;
            dg_profiling.Columns["Customer_Phone"].Visible = false;
            dg_profiling.Columns["Customer_Adress"].Visible = false;
            dg_profiling.Columns["DateOfRegistration"].HeaderText = "Проблема зарегистрирована";
            dg_profiling.Columns["Status"].Visible = false;
            dg_profiling.Columns["DateOfStatus"].Visible = false;
            dg_profiling.Columns["Resume"].Visible = false;
            dg_profiling.Columns["Worker_Id"].Visible = false;

            dg_profiling.Columns["Id1"].Visible = false;
            dg_profiling.Columns["Login"].Visible = false;
            dg_profiling.Columns["Password"].Visible = false;
            dg_profiling.Columns["Name"].Visible = false;
            dg_profiling.Columns["Patronymic"].Visible = false;
            dg_profiling.Columns["Skills"].Visible = false;
            dg_profiling.Columns["Score"].Visible = false;
            dg_profiling.Columns["Surname"].Visible = false ;

            dg_profiling.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;

        }

        private void upDateWindow()
        {
            var mas = dg_profiling.SelectedRows;
            var candidat = mas[0].Cells[0].FormattedValue.ToString();

            var con = new SqlConnection(ConnectionString);
            con.Open();
            var request = "SELECT Description FROM Problem_Table WHERE Id = " + candidat + "";
            var com = new SqlCommand(request, con);
            var last = com.ExecuteScalar();
            p_Description.Text = last.ToString();

            request = "SELECT Worker_Id FROM Problem_Table WHERE Id = " + candidat + "";
            com = new SqlCommand(request, con);
            last = com.ExecuteScalar();
            string wTest = last.ToString();
            if (wTest != "")
            {
                int wID = (int)last;

                request = "SELECT Name FROM Worker_Table WHERE Id = " + wID + "";
                com = new SqlCommand(request, con);
                last = com.ExecuteScalar();
                p_Worker_Name.Text = last.ToString();

                request = "SELECT Surname FROM Worker_Table WHERE Id = " + wID + "";
                com = new SqlCommand(request, con);
                last = com.ExecuteScalar();
                p_Worker_Surname.Text = last.ToString();

                request = "SELECT Patronymic FROM Worker_Table WHERE Id = " + wID + "";
                com = new SqlCommand(request, con);
                last = com.ExecuteScalar();
                p_Worker_Patronymic.Text = last.ToString();

                request = "SELECT Skills FROM Worker_Table WHERE Id = " + wID + "";
                com = new SqlCommand(request, con);
                last = com.ExecuteScalar();
                p_Worker_Skills.Text = last.ToString();

                request = "SELECT Score FROM Worker_Table WHERE Id = " + wID + "";
                com = new SqlCommand(request, con);
                last = com.ExecuteScalar();
                p_Worker_Rating.Text = last.ToString();
            }
            con.Close();
        }
        private void ViewMobileSpecProblemForm_Load(object sender, EventArgs e)
        {
            updateData();
            upDateWindow();
        }

        private void dg_profiling_CellALlContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
            upDateWindow();
        }

        private void dg_profiling_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            updateData();
            upDateWindow();
        }

        private void CHANGE_STATUS_OG_PROBLEM_Click(object sender, EventArgs e)
        {
            var form = new ChangeStatusOfProblem();
            var mas = dg_profiling.SelectedRows;
            var candidat = mas[0].Cells[0].FormattedValue.ToString();

            var con = new SqlConnection(ConnectionString);
            con.Open();
            var request = "SELECT Status FROM Problem_Table WHERE Id = " + candidat + "";
            var com = new SqlCommand(request, con);
            var last = com.ExecuteScalar();
            form.CStatus_Combo_box.Text = last.ToString();

            request = "SELECT Resume FROM Problem_Table WHERE Id = " + candidat + "";
            com = new SqlCommand(request, con);
            last = com.ExecuteScalar();
            form.CResume.Text = last.ToString();

            con.Close();
 
            if (form.ShowDialog() == DialogResult.OK)
            {
                con = new SqlConnection(ConnectionString);
                con.Open();

                request = "UPDATE Problem_Table SET Status= N'" + form.CStatus_Combo_box.Text + "' WHERE Id='" + candidat + "'";
                com = new SqlCommand(request, con);
                com.ExecuteNonQuery();

                request = "UPDATE Problem_Table SET Resume = N'" + form.CResume.Text + "' WHERE Id='" + candidat + "'";
                com = new SqlCommand(request, con);
                com.ExecuteNonQuery();

                DateTime myDateTime = DateTime.Now;
                string sqlFormattedDate = myDateTime.ToString("yyyy-MM-dd HH:mm:ss.fff");

                request = "UPDATE Problem_Table SET DateOfStatus= '" + sqlFormattedDate + "' WHERE Id='" + candidat + "'";
                com = new SqlCommand(request, con);
                com.ExecuteNonQuery();

                con.Close();
                updateData();
                upDateWindow();
            }
            
        }

        private void CANCEL_BUTTON_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
