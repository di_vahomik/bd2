﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Errors
{
    public partial class RatingForm : Form
    {
        private readonly string ConnectionString = Form1.ConnectionString;
        public RatingForm()
        {
            InitializeComponent();

            textBox1.Text = "02.03.2020";
            textBox2.Text = "06.07.2020";
        }
        private void UpdateData()
        {
            CultureInfo MyCultureInfo = new CultureInfo("en-US");
            var con = new SqlConnection(ConnectionString);
            try
            {
                DateTime date1 = DateTime.Parse(textBox1.Text, MyCultureInfo);
                string sqlFormattedDate1 = date1.ToString("MM.dd.yyyy");
                DateTime date2 = DateTime.Parse(textBox2.Text, MyCultureInfo);
                string sqlFormattedDate2 = date2.ToString("MM.dd.yyyy");
                textBox1.Text = sqlFormattedDate1;
                textBox2.Text = sqlFormattedDate2;

                /////////////////////////////////////////////
                /////////////////////// 1 ///////////////////
                /////////////////////////////////////////////

                var request = "SELECT SUM(CASE WHEN Status = 'registered' then 1 else null end) AS s_reg, SUM (CASE WHEN Status = 'delayed' then 1 else null end) AS s_del, SUM (CASE WHEN Status = 'cannot be solved' OR Status = 'registrated' OR Status = 'delayed' OR Status = 'responsible person appointed' OR Status = 'accepted for execution' then 1 else null end) AS s_notsolved, SUM (CASE WHEN Status = 'eliminated' then 1 else null end) AS s_elim,  SUM (CAST(CASE WHEN Status = 'registered' then 1 else null end AS DEC))/ NULLIF(COUNT(CASE WHEN Status = 'eliminated' then 1 else null end), 0)AS otn FROM Problem_Table WHERE DateOfRegistration  BETWEEN '" + textBox1.Text + "' AND '" + textBox2.Text + "'";

                var adapter = new SqlDataAdapter(request, ConnectionString);
                var table = new DataTable();
                adapter.Fill(table);

                Rating_dataGridView.DataSource = table;
                Rating_dataGridView.Columns["s_elim"].HeaderText = "Кол-во устраненных запросов:";
                Rating_dataGridView.Columns["s_reg"].HeaderText = "Кол-во поступивших запросов:";
                Rating_dataGridView.Columns["s_del"].HeaderText = "Кол-во отложенных запросов:";
                Rating_dataGridView.Columns["s_notsolved"].HeaderText = "Кол-во не устраненных проблем:";
                Rating_dataGridView.Columns["otn"].HeaderText = "Отношение кол-ва поступивших запросов к устраненным:";

                /////////////////////////////////////////////
                /////////////////////// 2 ///////////////////
                /////////////////////////////////////////////

                request = "SELECT Problem_Table.Customer_Source, COUNT(Problem_Table.TypeOfEquipment) AS c_reg_probl FROM Problem_Table WHERE DateOfRegistration  BETWEEN '" + textBox1.Text + "' AND '" + textBox2.Text + "' GROUP BY Problem_Table.Customer_Source ORDER BY c_reg_probl DESC";

                adapter = new SqlDataAdapter(request, ConnectionString);
                table = new DataTable();
                adapter.Fill(table);

                client_rat_dgv.DataSource = table;
                client_rat_dgv.Columns["Customer_Source"].HeaderText = "Клиент";
                client_rat_dgv.Columns["c_reg_probl"].HeaderText = "Кол-во запросов:";


                /////////////////////////////////////////////
                /////////////////////// 3 ///////////////////
                /////////////////////////////////////////////

                request = "SELECT Problem_Table.TypeOfEquipment, COUNT(Problem_Table.TypeOfEquipment) AS c_reg_probl FROM Problem_Table WHERE DateOfRegistration  BETWEEN '" + textBox1.Text + "' AND '" + textBox2.Text + "' GROUP BY Problem_Table.TypeOfEquipment ORDER BY c_reg_probl DESC";

                adapter = new SqlDataAdapter(request, ConnectionString);
                table = new DataTable();
                adapter.Fill(table);

                type_equipment_dgv.DataSource = table;
                type_equipment_dgv.Columns["TypeOfEquipment"].HeaderText = "Проблемное оборудование:";
                type_equipment_dgv.Columns["c_reg_probl"].HeaderText = "Кол-во запросов:";


                /////////////////////////////////////////////
                /////////////////////// 4 ///////////////////
                /////////////////////////////////////////////


                var newrequest = "SELECT concat(Surname, ' ', Name, ' ',  Patronymic) as ful, COUNT(Problem_Table.Worker_id) as work FROM (Worker_Table INNER JOIN Problem_Table ON Worker_Table.Id=Problem_Table.Worker_id) WHERE DateOfRegistration  BETWEEN '" + textBox1.Text + "' AND '" + textBox2.Text + "' GROUP BY concat(Surname, ' ', Name, ' ', Patronymic) ORDER BY work ASC";
                var newcom = new SqlDataAdapter(newrequest, ConnectionString);
                table = new DataTable();
                newcom.Fill(table);
                worker_rating_dgv.DataSource = table;
                worker_rating_dgv.Columns["ful"].HeaderText = "ФИО мобильного специалиста:";
                worker_rating_dgv.Columns["work"].HeaderText = "Кол-во запросов:";
            }
            catch
            {
                MessageBox.Show("Введен неверный формат даты\nПравильный формат: MM.dd.YYYY", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            finally
            {
                con.Close();
            }

        }

        private void RatingForm_Load(object sender, EventArgs e)
        {
            UpdateData();
        }

     
        private void Rating_dataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            UpdateData();
        }

        private void UpdateDate_Click(object sender, EventArgs e)
        {
            UpdateData();
        }

        private void CANCEL_BUTTON_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
