﻿namespace Errors
{
    partial class ProfillingAddProfilForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.linkW_Rating = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.linkW_Skills = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.linkP_Priority = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.OK_BUTTON = new System.Windows.Forms.Button();
            this.cb_linkWorker = new System.Windows.Forms.ComboBox();
            this.CANCEL_BUTTON = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // linkW_Rating
            // 
            this.linkW_Rating.Location = new System.Drawing.Point(446, 44);
            this.linkW_Rating.Name = "linkW_Rating";
            this.linkW_Rating.ReadOnly = true;
            this.linkW_Rating.Size = new System.Drawing.Size(42, 20);
            this.linkW_Rating.TabIndex = 27;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(324, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(116, 13);
            this.label3.TabIndex = 26;
            this.label3.Text = "Рейтинг специалиста";
            // 
            // linkW_Skills
            // 
            this.linkW_Skills.Location = new System.Drawing.Point(134, 44);
            this.linkW_Skills.Name = "linkW_Skills";
            this.linkW_Skills.ReadOnly = true;
            this.linkW_Skills.Size = new System.Drawing.Size(184, 20);
            this.linkW_Skills.TabIndex = 25;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 13);
            this.label2.TabIndex = 24;
            this.label2.Text = "Навыки специалиста";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 73);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(116, 13);
            this.label5.TabIndex = 23;
            this.label5.Text = "Приоритет проблемы";
            // 
            // linkP_Priority
            // 
            this.linkP_Priority.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.linkP_Priority.FormattingEnabled = true;
            this.linkP_Priority.Items.AddRange(new object[] {
            "незначительная",
            "серьезная",
            "критическая"});
            this.linkP_Priority.Location = new System.Drawing.Point(134, 70);
            this.linkP_Priority.Name = "linkP_Priority";
            this.linkP_Priority.Size = new System.Drawing.Size(354, 21);
            this.linkP_Priority.TabIndex = 22;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 13);
            this.label1.TabIndex = 21;
            this.label1.Text = "Моб. специалист";
            // 
            // OK_BUTTON
            // 
            this.OK_BUTTON.Location = new System.Drawing.Point(244, 97);
            this.OK_BUTTON.Name = "OK_BUTTON";
            this.OK_BUTTON.Size = new System.Drawing.Size(139, 23);
            this.OK_BUTTON.TabIndex = 20;
            this.OK_BUTTON.Text = "Назначить";
            this.OK_BUTTON.UseVisualStyleBackColor = true;
            this.OK_BUTTON.Click += new System.EventHandler(this.OK_BUTTON_Click);
            // 
            // cb_linkWorker
            // 
            this.cb_linkWorker.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_linkWorker.FormattingEnabled = true;
            this.cb_linkWorker.Location = new System.Drawing.Point(111, 17);
            this.cb_linkWorker.Name = "cb_linkWorker";
            this.cb_linkWorker.Size = new System.Drawing.Size(377, 21);
            this.cb_linkWorker.TabIndex = 19;
            this.cb_linkWorker.SelectedIndexChanged += new System.EventHandler(this.Cb_linkWorker_SelectedIndexChanged);
            // 
            // CANCEL_BUTTON
            // 
            this.CANCEL_BUTTON.Location = new System.Drawing.Point(404, 97);
            this.CANCEL_BUTTON.Name = "CANCEL_BUTTON";
            this.CANCEL_BUTTON.Size = new System.Drawing.Size(96, 22);
            this.CANCEL_BUTTON.TabIndex = 28;
            this.CANCEL_BUTTON.Text = "Отмена";
            this.CANCEL_BUTTON.UseVisualStyleBackColor = true;
            this.CANCEL_BUTTON.Click += new System.EventHandler(this.CANCEL_BUTTON_Click);
            // 
            // ProfillingAddProfilForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(516, 131);
            this.Controls.Add(this.CANCEL_BUTTON);
            this.Controls.Add(this.linkW_Rating);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.linkW_Skills);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.linkP_Priority);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.OK_BUTTON);
            this.Controls.Add(this.cb_linkWorker);
            this.Name = "ProfillingAddProfilForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Назначить мобильного специалиста";
            this.Load += new System.EventHandler(this.ProfillingAddProfilForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox linkW_Rating;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox linkW_Skills;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.ComboBox linkP_Priority;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button OK_BUTTON;
        public System.Windows.Forms.ComboBox cb_linkWorker;
        private System.Windows.Forms.Button CANCEL_BUTTON;
    }
}