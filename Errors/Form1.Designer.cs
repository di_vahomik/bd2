﻿namespace Errors
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.Registration_button = new System.Windows.Forms.Button();
            this.Profilling_button = new System.Windows.Forms.Button();
            this.MobileSpec_button = new System.Windows.Forms.Button();
            this.MonQuality_button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Registration_button
            // 
            this.Registration_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Registration_button.ForeColor = System.Drawing.SystemColors.InfoText;
            this.Registration_button.Image = ((System.Drawing.Image)(resources.GetObject("Registration_button.Image")));
            this.Registration_button.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Registration_button.Location = new System.Drawing.Point(17, 24);
            this.Registration_button.Name = "Registration_button";
            this.Registration_button.Size = new System.Drawing.Size(293, 54);
            this.Registration_button.TabIndex = 0;
            this.Registration_button.Text = "Регистратор";
            this.Registration_button.UseVisualStyleBackColor = true;
            this.Registration_button.Click += new System.EventHandler(this.Registration_button_Click);
            // 
            // Profilling_button
            // 
            this.Profilling_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Profilling_button.ForeColor = System.Drawing.Color.OrangeRed;
            this.Profilling_button.Image = ((System.Drawing.Image)(resources.GetObject("Profilling_button.Image")));
            this.Profilling_button.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Profilling_button.Location = new System.Drawing.Point(316, 24);
            this.Profilling_button.Name = "Profilling_button";
            this.Profilling_button.Size = new System.Drawing.Size(293, 54);
            this.Profilling_button.TabIndex = 1;
            this.Profilling_button.Text = "Профилировщик";
            this.Profilling_button.UseVisualStyleBackColor = true;
            this.Profilling_button.Click += new System.EventHandler(this.Profilling_button_Click);
            // 
            // MobileSpec_button
            // 
            this.MobileSpec_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.MobileSpec_button.ForeColor = System.Drawing.Color.LimeGreen;
            this.MobileSpec_button.Image = ((System.Drawing.Image)(resources.GetObject("MobileSpec_button.Image")));
            this.MobileSpec_button.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.MobileSpec_button.Location = new System.Drawing.Point(17, 84);
            this.MobileSpec_button.Name = "MobileSpec_button";
            this.MobileSpec_button.Size = new System.Drawing.Size(293, 53);
            this.MobileSpec_button.TabIndex = 2;
            this.MobileSpec_button.Text = "Мобильный специалист";
            this.MobileSpec_button.UseVisualStyleBackColor = true;
            this.MobileSpec_button.Click += new System.EventHandler(this.MobileSpec_button_Click);
            // 
            // MonQuality_button
            // 
            this.MonQuality_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.MonQuality_button.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.MonQuality_button.Image = ((System.Drawing.Image)(resources.GetObject("MonQuality_button.Image")));
            this.MonQuality_button.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.MonQuality_button.Location = new System.Drawing.Point(316, 84);
            this.MonQuality_button.Name = "MonQuality_button";
            this.MonQuality_button.Size = new System.Drawing.Size(293, 53);
            this.MonQuality_button.TabIndex = 3;
            this.MonQuality_button.Text = "Мониторинг качества";
            this.MonQuality_button.UseVisualStyleBackColor = true;
            this.MonQuality_button.Click += new System.EventHandler(this.MonQuality_button_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(627, 155);
            this.Controls.Add(this.MonQuality_button);
            this.Controls.Add(this.MobileSpec_button);
            this.Controls.Add(this.Profilling_button);
            this.Controls.Add(this.Registration_button);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Обслуживание телекомунникационного оборудования";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Registration_button;
        private System.Windows.Forms.Button Profilling_button;
        private System.Windows.Forms.Button MobileSpec_button;
        private System.Windows.Forms.Button MonQuality_button;
    }
}

